var sprintf = require('sprintf-js').sprintf;

module.exports = function (shipit) {

    require('shipit-deploy')(shipit);

    shipit.initConfig({
        default: {
            repositoryUrl: 'git@bitbucket.org:norkcode/quiz-client.git',
            keepReleases: 5,
            workspace: '/tmp/quiz-client',
            key: '/Users/maksimnorkin/.ssh/id_rsa.pub',
            deployTo: '/var/www/prod/quiz-client',
            dirToCopy: 'build',
            shallowClone: true
        },
        prod: {
            servers: 'root@165.227.129.215'
        }
    });

    shipit.on('fetched', function () {
        shipit.start('react:build');

    });

    shipit.blTask('react:build', function () {
        return shipit['local'](sprintf('node -v && cd %s && yarn install && yarn build', shipit.config.workspace));
    })
};