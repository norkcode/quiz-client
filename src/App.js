import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import { Row, Col, Layout, Spin, Radio, Form, Button } from 'antd';
import _ from 'lodash';

const {Header, Content} = Layout;
const RadioGroup = Radio.Group;
const FormItem = Form.Item;

class QuestionItemCollection extends Component {
    render() {
        const { getFieldDecorator } = this.props.form;

        let rows = [];

        this.props.list.forEach((item) => {

            let answers = [];

            item.answers.forEach((answer) => {
                answers.push(
                    <Radio key={answer._id} value={answer._id}>{answer.title}</Radio>
                );
            });

            rows.push(
                <div key={item._id}>
                    <FormItem label={item.title}>
                        {getFieldDecorator(item._id, {
                            rules: [
                                {
                                    type: 'string',
                                    required: true,
                                    message: 'Please select your choice'
                                }
                            ]
                        })(
                            <RadioGroup>
                                {answers}
                            </RadioGroup>
                        )}
                    </FormItem>
                </div>
            );
        });
        return (
            <div>
                {rows}
            </div>
        );
    }
}

class QuizForm extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isLoading: true,
            isComplete: false,
            score: 0,
            questions: []
        }
    };

    componentDidMount () {
        return fetch('/quizzes/' + this.props.match.params.id, {accept: 'application/json'})
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    title: responseJson.title,
                    questions: responseJson.questions
                });
            });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let correct = _.map(values, (answerId, questionId) => {
                    let question = _.find(this.state.questions, function (question) {
                        return question._id === questionId;
                    });
                    return question.correct._id === answerId;
                });
                this.setState({
                    isComplete: true,
                    score: _.sum(correct)
                })
            }
        });
    };

    render() {
        if (this.state.isLoading) {
            return (
                <div>
                    <Spin />
                </div>
            )
        }

        if (this.state.isComplete) {
            return (
                <div>
                    <h1>Test is complete!</h1>
                    <h2>
                        Score is {this.state.score}/{this.state.questions.length}
                    </h2>
                    <br />
                    <Link to="/">take another test</Link>
                </div>
            )
        }
        return (
            <div>
                <Form onSubmit={this.handleSubmit}>
                    <h1>{this.state.title} quiz</h1>
                    <br />
                    <QuestionItemCollection list={this.state.questions} form={this.props.form} />
                    <br />
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Check results
                    </Button>
                    <br />
                    <br />
                    <Link to="/">back</Link>
                </Form>
            </div>
        )
    }
}

const WrappedQuizForm = Form.create()(QuizForm);

class QuizLinkCollection extends Component {
    render() {
        let rows = [];
        this.props.list.forEach((item) => {
            rows.push(
                <ul key={item._id}>
                    <li>
                        <Link to={'/quiz/' + item._id}>{item.title}</Link>
                    </li>
                </ul>
            );
        });

        return (
            <div>
                {rows}
            </div>
        )
    }

}

class Home extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isLoading: true,
            quizzes: []
        }
    };

    componentDidMount() {
        return fetch('/quizzes', {accept: 'application/json'})
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    quizzes: responseJson.docs
                });
            })
            .catch(error => {
                // response was not as expected
            });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <div>
                    <Spin />
                </div>
            );
        }
        return (
            <div>
                <h2>Developers quiz</h2>
                <p>Check your luck and win prizes</p>
                <p>Click on the following quizzes to test your skills</p>
                <br />
                <QuizLinkCollection list={this.state.quizzes} />
            </div>
        )
    }
}

const App = () => (
    <Router>
        <div>
            <Header style={{ background: '#fff', padding: 0 }} />
            <Content>
                <Row>
                    <Col offset={2} span={20}>
                        <Route exact path="/" component={Home} />
                        <Route path="/quiz/:id" component={WrappedQuizForm} />
                    </Col>
                </Row>
            </Content>
        </div>
    </Router>
);

export default App;