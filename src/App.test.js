import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders empty without crashing', () => {
    window.fetch = jest.fn(() => Promise.resolve({json: () => {}}));

    const div = document.createElement('div');
    ReactDOM.render(<App />, div);

    expect(window.fetch).toBeCalled();
});

it('renders available list of questions', () => {
    window.fetch = jest.fn(() => Promise.resolve({json: () => {
        return {
            "docs": [
                {
                    "_id": 1,
                    "title": "Test Quiz"
                }
            ]
        }
    }}));

    const div = document.createElement('div');
    ReactDOM.render(<App />, div);

    expect(window.fetch).toBeCalled();
});